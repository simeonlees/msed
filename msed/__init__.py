import sys
from PyQt4 import QtGui
from msed.taskrunner import TaskRunner
from msed.pedestriandetector import PedestrianDetector
from msed.db import DB
from msed.gui import Gui


trConfig = '../config/taskrunner.conf'
pdConfig = '../config/pedestriandetector.conf'
dbConfig = '../config/database.conf'

class App(QtGui.QApplication):
    def __init__(self, sys_argv):
        super(App, self).__init__(sys_argv)
        print('starting main')
        pd = PedestrianDetector(pdConfig)
        db = DB(dbConfig)
        tr = TaskRunner(trConfig, db, pd)
        self.gui = Gui(tr)
        self.gui.show()

if __name__ == "__main__":
    app = App(sys.argv)
    sys.exit(app.exec_())
