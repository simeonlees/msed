from PyQt4 import QtGui
from msed.generated_gui import Ui_MainWindow

class Gui(QtGui.QMainWindow):

    def __init__(self, task_runner):
        super(Gui, self).__init__()
        self.init_ui()
        self.task_runner = task_runner

    def init_ui(self):
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.startButton.clicked.connect(self.start_task_runner)
        self.ui.stopButton.clicked.connect(self.stop_task_runner)

    def start_task_runner(self):
        self.ui.startButton.setEnabled(False)
        self.task_runner.start()
        self.ui.stopButton.setEnabled(True)

    def stop_task_runner(self):
        self.ui.stopButton.setEnabled(False)
        self.task_runner.stop()
        self.ui.startButton.setEnabled(True)
