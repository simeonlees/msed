from imutils.object_detection import non_max_suppression
import numpy as np
import cv2
import imutils

class PedestrianDetector:

    __config = {}

    __hog_descriptor = None
    __previous_detection_ranges = []

    def __init__(self, configPath):

        exec(compile(open(configPath, "rb").read(), configPath, 'exec'), self.__config)
        self.__hog_descriptor = cv2.HOGDescriptor()
        self.__hog_descriptor.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())


    def detect(self, filepath):
        image = cv2.imread(filepath, cv2.IMREAD_COLOR)
        
        image = imutils.resize(image, width=min(1000, image.shape[1]))

        (rects, weights) = self.__hog_descriptor.detectMultiScale(image, winStride=(2, 2), padding=(8,8), scale=1.05) # # scale 1.05

        rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
        cleaned_detections = non_max_suppression(rects, probs=None, overlapThresh=0.7)

        motionless_pedestrians = self.calculate_motionless_pedestrians(cleaned_detections)

        return len(cleaned_detections), motionless_pedestrians


    def calculate_motionless_range(self, detection):
        xRange = (detection[2] - detection[0]) * self.__config['motionlessXRange']
        yRange = (detection[3] - detection[1]) * self.__config['motionlessYRange']
        xCenter = detection[0] + ((detection[2] - detection[0])/2)
        yCenter = detection[1] + ((detection[3] - detection[1])/2)
        return [xCenter - xRange, yCenter - yRange, xCenter + xRange, yCenter + yRange]

    def point_is_in_ranges(self, pointX, pointY, ranges):
        for range in ranges:
            if range[2] >= pointX >= range[0] and range[3] >= pointY >= range[1]:
                return True
        return False

    def calculate_motionless_pedestrians(self, current_detections):
        motionless_pedestrians = 0
        if self.__previous_detection_ranges:
            for cd in current_detections:
                centerX = cd[0] + ((cd[2] - cd[0])/2)
                centerY = cd[1] + ((cd[3] - cd[1])/2)
                if self.point_is_in_ranges(centerX, centerY, self.__previous_detection_ranges):
                    motionless_pedestrians += 1
        self.__previous_detection_ranges = [self.calculate_motionless_range(d) for d in current_detections]
        return motionless_pedestrians

