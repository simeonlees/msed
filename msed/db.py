import MySQLdb
import time

class DB:

    config = {}

    def __init__(self, configPath):

        # Loads in config file
        exec(compile(open(configPath, "rb").read(), configPath, 'exec'), self.config)
        self.connect()

    def connect(self):
        self.db = MySQLdb.connect(self.config['hostname'], self.config['username'], self.config['password'], self.config['database'])
        self.cursor = self.db.cursor()

    def getCon(self):
        return self.db

    def insert_record(self, datetime, number_of_detections, number_motionless):
        formattedTime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(datetime))
        try:
            sql = "INSERT INTO {0} VALUES (NULL,'{1}','{2}','{3}')".format(self.config['detection_records_table_name'], formattedTime, number_of_detections, number_motionless)
            self.cursor.execute(sql)
            self.db.commit()
        except:
            self.db.rollback()