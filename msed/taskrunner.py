import os
import time
from threading import Thread
from multiprocessing import Queue

class TaskRunner:

    __config = {}


    def __init__(self, configPath, db, pd):

        # Loads in config file
        exec(compile(open(configPath, "rb").read(), configPath, 'exec'), self.__config)

        # Initialises Pedestrian Detector
        self.pd = pd
        self.db = db
        self.isRunning = False
        self.imageQueue = Queue()
        self.imagesAddedToQueue = []



    def checkForNewImages(self, imageQueue, interval):
        i = 0
        while self.isRunning:
            files = os.listdir(self.__config['imageFolderPath'])
            if len(files) > 0:
                for file in files:
                    if file not in self.imagesAddedToQueue:
                        print('Added {0} to queue and list!'.format(file))
                        self.imageQueue.put(file)
                        self.imagesAddedToQueue.append(file)
            for image in self.imagesAddedToQueue:
                if image not in files:
                    self.imagesAddedToQueue.remove(image)
                    print('Removed {0} from list!'.format(file))

            print('Checking for images...'+str(i))
            time.sleep(interval)
            i += 1

    def processImagesInQueue(self, imageQueue, interval):
        while self.isRunning:
            if not imageQueue.empty():
                image = imageQueue.get()
                image_path = os.path.join(self.__config['imageFolderPath'], image)
                image_datetime = os.path.getmtime(image_path)
                number_of_detections, number_motionless = self.pd.detect(image_path)
                print(number_of_detections)
                print(number_motionless)
                continue
                self.db.insert_record(image_datetime, number_of_detections, number_motionless)
                os.remove(image_path)
            else:
                time.sleep(interval)

    def start(self):
        print('starting task runner')
        self.isRunning = True
        checkForNewImagesThread = Thread(target=self.checkForNewImages,
                                         args=(self.imageQueue, self.__config['imageCheckInterval']))
        checkForNewImagesThread.start()
        imageProcessingThread = Thread(target=self.processImagesInQueue,
                                         args=(self.imageQueue, self.__config['imageProcessInterval']))
        imageProcessingThread.start()

        # totalPedestrians = self.__pd.detect('imagebuffer/frame17010610540300.jpg')
        # print('Total Pedestrians: '+ str(totalPedestrians))

    def stop(self):
        print('stopping task runner')
        self.isRunning = False

        # totalPedestrians = self.__pd.detect('imagebuffer/frame17010610540300.jpg')
        # print('Total Pedestrians: '+ str(totalPedestrians))

