"""
MOCK DATA GENERATOR

If updating table, first run TRUNCATE TABLE 'mock_detections' command on database

"""


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime
from msed.db import DB
from sqlalchemy import create_engine
import datetime as dt
import random
import math

engine = create_engine("mysql+mysqldb://root:"+''+"@localhost/msed")

no_of_days = 91# 61

start = datetime.strptime('Mar 1 2017  7:00AM', '%b %d %Y %I:%M%p')
end = datetime.strptime('Mar 1 2017  11:00PM', '%b %d %Y %I:%M%p')

random_spread = 0.5

master = pd.DataFrame()

for i in range(no_of_days):

    time_index = pd.date_range(start, end, freq="10s")

    people_count = []

    # Random element between days
    day_variance = random.uniform(0.7, 1.3)

    # Increase detections if weekend
    if 5 <= start.weekday() <= 6:
        day_variance *= 1.6

    # 7 - 9
    for num in range(0,720):
        people_count.append(math.floor(random.uniform(
            (4 + num // 60)*(1-random_spread),
            (4 + num // 60) * (1 + random_spread)
        )*day_variance))

    # 9 - 12
    for num in range(0,1080):
        people_count.append(math.floor(random.uniform(
            (6 + num // 200)*(1-random_spread),
            (6 + num // 200) * (1 + random_spread)
        )*day_variance))

    # 12 - 1
    for num in range(360,720):
        people_count.append(math.floor(random.uniform(
            (16 + num // 200)*(1-random_spread),
            (16 + num // 200) * (1 + random_spread)
        )*day_variance))

    # 1 - 2
    for num in reversed(range(360,720)):
        people_count.append(math.floor(random.uniform(
            (10 + num // 200)*(1-random_spread),
            (10 + num // 200) * (1 + random_spread)
        )*day_variance))

    # 2 - 5
    for num in reversed(range(0,1080)):
        people_count.append(math.floor(random.uniform(
            (6 + num // 400)*(1-random_spread),
            (6 + num // 400) * (1 + random_spread)
        )*day_variance))

    # 5 - 7
    for num in reversed(range(360,1080)):
        people_count.append(math.floor(random.uniform(
            (4 + num // 70)*(1-random_spread),
            (4  + num // 70) * (1 + random_spread)
        )*day_variance))

    # 7 - 11
    for num in reversed(range(800,2241)):
        people_count.append(math.floor(random.uniform(
            (0 + num // 200) * (1 - random_spread),
            (0 + num // 200) * (1 + random_spread)
        )*day_variance))

    print(len(people_count))

    df = pd.DataFrame({'no_of_detections':people_count, 'datetime':time_index})
    df['no_motionless'] = np.floor(df['no_of_detections'] / 5).astype(int)

    if len(master) > 0:
        master = master.append(df, ignore_index=True)
    else:
        master = df

    start = start + dt.timedelta(days=1)
    end = end + dt.timedelta(days=1)

master.reset_index(inplace=True)
del master['index']
print(master)

master.to_sql(con=engine, name='mock_detections', if_exists='replace', index_label='id')





