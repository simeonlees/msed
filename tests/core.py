import unittest
import msed


class CoreTestSuite(unittest.TestCase):
    """Basic test cases."""

    trConfig = 'config/taskrunner.conf'
    pdConfig = 'config/pedestriandetector.conf'
    dbConfig = 'config/database.conf'

    # db = msed.DB(self.dbConfig)
    # tr = msed.TaskRunner(self.trConfig, db, pd)

    def test_detect_pedestrians(self):
        """ Tests that PDC detects expected number of pedestrians """

        # Arrange
        pd = msed.PedestrianDetector(self.pdConfig)

        # Act
        pedestrians, motionless = pd.detect('test_pedestrians.jpg')

        # Assert
        self.assertEqual(pedestrians, 7)

    def test_detect_motionless(self):
        pd = msed.PedestrianDetector(self.pdConfig)
        pedestrians, motionless = pd.detect('test_pedestrians.jpg')
        pedestrians, motionless = pd.detect('test_pedestrians.jpg')
        self.assertEqual(motionless, 7)

    def test_motionless_range(self):
        pd = msed.PedestrianDetector(self.pdConfig)
        range = pd.calculate_motionless_range([4,4,8,6])
        self.assertEqual(range, [3.2, 4.2, 8.8, 5.8])

    def test_point_is_in_ranges(self):
        pd = msed.PedestrianDetector(self.pdConfig)
        in_ranges = pd.point_is_in_ranges(4,4,[[2,2,6,6]])
        self.assertTrue(in_ranges)

    def test_point_not_in_ranges(self):
        pd = msed.PedestrianDetector(self.pdConfig)
        in_ranges = pd.point_is_in_ranges(6,7,[[2,2,6,6],[8,8,9,10]])
        self.assertFalse(in_ranges)



if __name__ == '__main__':
    unittest.main()